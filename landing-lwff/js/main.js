jQuery(function($){
	$('.carousel').carousel({
		interval: false
	});
	
	$('.btn-details').click( function(e){
		var rowid = $(this).attr( 'rel' );
		$( '#icon-' + rowid ).toggleClass( 'icon-plus-sign icon-minus-sign' );
		$( '#details-' + rowid ).slideToggle();
		e.preventDefault();
	});
});
